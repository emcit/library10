<?php
//Here we created our books table using the migration functionality
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
public function up()
	{
		Schema::create('books', function($table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('isbn')->unique();
			$table->date('publish_date');
			$table->integer('author_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('books');
	}
	

}