<?php
namespace larkin\repository\impl;

use larkin\repository\LoanRepository;
use larkin\mapper\LoanRowMapper;
use Illuminate\Support\Facades\DB;

class DbLoanRepository implements LoanRepository {

	private $rowMapper;
	
	function __construct() {
		$this->rowMapper = new LoanRowMapper();
	}
	
	function getById($id) {
		$loan = DB::table('loans')->where('id', $id);
		return $this->rowMapper->mapRow($loan);
	}
	
	function getAll() {
		$loans = DB::table('loans')->get();
		$rowMapper = new LoanRowMapper();
		return $this->rowMapper->mapRowToArray($loans);
	}
}