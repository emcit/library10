<?php
namespace larkin\repository\impl;

class PdoAuthorRepository implements AuthorRepository {
	
	private $datasource;
	
	function __construct(PDO $datasource) {
		$this->datasource = $datasource;
	}
	
	function getById($id) {
		$statement = $this->datasource->prepare("SELECT * FROM authors WHERE id = ?");
		$statement->bind(1,$id);
		$author = $statement->execute();
		return AuthorRowMapper($author);
	}

	function findAll() {
		$authors = $this->datasource->query("SELECT * FROM authors");
		return AuthorRowMapper($authors);
	}
}