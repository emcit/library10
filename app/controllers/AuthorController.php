<?php
/*
 * This controller class is here to enable future development of the Library.
 * We left this controller in our project to illustrate how we would implement the service, repository, controller
 * and model layers if layering our project.  This would have been done if we had more time to follow this layering principle
 * 
 */
use larkin\service\AuthorService;
class AuthorController extends BaseController {
	private $authorService;
	function __construct(AuthorService $authorService) {
		$this->authorService = $authorService;
	}
	public function listAuthors() {
		$authors = $this->authorService->getAll();
		return View::make('listAuthors')->with('authors', $authors);
	}
	public function viewAuthor() { // must get a parameter here...
		return View::make('viewAuthor')->with('authors',
				$this->authorService->getById(1));
	}
	
	public function listIrishAuthors() {
		$authors = $this->authorService->getIrish();
		return View::make('listAuthors')->with('authors', $authors);
	}

}