<?php

/* This is a resource controller with the standard functions that are created with a resource controller
 * This class is controlling what is displayed in the admin view which has a link to display a fines report and also has two forms which appear 
 * when the administator logs in.
 * So we are allowing the administrator to update the loan period, the fine rate and the book allowance for all members.
 * In this class we are using the index and show functions.*/

use Illuminate\Support\Facades\Redirect;
class AdminController extends \BaseController {

	function __construct() {
		
		
	}
	/*This function is querying our database to display the results for the fine report with the users with outstanding fines and fine amount
	and displaying this data in the admin.index view*/
	public function index()
	{
		$result = DB::select('SELECT count(users.id) as times, users.name, loans.userid, 
				sum(loans.Fine_amount) as fines FROM loans, users where loans.userid = users.id  
				group by loans.userid order by fines desc');
		
		return View::make ( 'admin.index' )->with ( 'result', $result );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::to('admin');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/*This function enables the administator to update the loan period from 2 weeks,
	 *the fine rate and as well as the book allowance for all members.
	 * */
	public function show($id)
	{
		DB::update('update loans set Loan_period = ?', array(Input::get ( 'loanperiod' )));
		DB::update('update loans set Fine_rate = ?', array(Input::get ( 'fine' )));
		DB::update('update users set Book_allowance = ?', array(Input::get ( 'ba' )));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}