<?php
/*
 * This is an example of a RESTful controller and is also a resource controller.
 * It has the standard controller functions index, edit, show and update.
 */
class BookController extends \BaseController {

	function __construct() {
		$this->beforeFilter('auth', array('except' =>
			array('index', 'show')));
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	//This function displays all the books in our index view class.
	public function index()
	{
		$books = Book::all();
		
		return View::make('book.index')->with('books', $books);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	//This function displays all the books with the edit link next to each title and is also linking author with the book
	public function show($id)
	{
		$book = Book::find($id);
		$author = $book->author;
		$allbooks = $author->books;
		
		$message = Session::get('message', '');
		
		return View::make('book.show')->with('book', $book)->with('author', $author)
			->with('allbooks', $allbooks)->with('message', $message);
	}
/**	public function showCategories(){
 		$book = Book::all();
 		$categories = null;
 		foreach ($book as $b){
 		$category = $b->category;
 		
 		$categories = array_merge($categories, $category->books);
 		}
 		return View::make('category.show')->with('category', $categories);
	}
	
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	//This function is sending the book information to the edit view
	public function edit($id)
	{
		$book = Book::find($id);
		return View::make('book.edit')->with('book', $book);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	//This function is allowing us to update the book information, such as ISBN and Title in the edit view and will display a new view of book is updated
	// Eloquent provides the validate method
	public function update($id)
	{
		$inputs = Input::all();
		$validator = Book::validate($inputs);
		
		if ($validator->passes()) {
			$book = Book::find($id);	
			
			$book->title = $inputs['title'];
			$book->isbn = $inputs['isbn'];
			$book->update();
		
			return Redirect::route('book.show', $id)->with('message', 'Book updated.');
		} else {
			return Redirect::route('book.edit', $id)->withErrors($validator);
		}
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

}