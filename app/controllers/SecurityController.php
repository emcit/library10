<?php
/*
 * This is a standard controller which is controlling our login, logout for members librarian and our administrator
 * and also facilitating the registration of a new user.
 * Based on the username entered in by the user we query our database for the 'role' of that user and
 * then bring them to that particular view be it a member view, librarian view or the admin view.
 */
class SecurityController extends BaseController {

	
	public function showLogin()
	{
		return View::make('login');
	}
	//This function is enabling our login for members,librarians and administrator and recognining new users bringing them to a registration form.
	public function doLogin()
	{
		
		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);
		$un = Input::get('username');
		$role = DB::table('users')->where('username',$un)->pluck('role');
		if (Auth::attempt($userdata)) {
			if($role=='admin')
				return Redirect::to('admin');
			else if($role=='librarian'){
				return Redirect::to('librarian');
			}
			else if($role=='member')
				return Redirect::to('member');
			else
				return Redirect::to('login');
		} else {
			return View::make('signup');
		}
	}
	
	//This function enables a user to logout
	public function doLogout()
	{
		Auth::logout(); 
		return Redirect::to('home'); 
	}

	//This brings up the signup view for new users
	public function doSignup()
	{
		return View::make('signup');
	}
	
	//IF we have a new user we extract all of the details they enter and we hash the password they enter for security purposes which is provided by the laravel framework
	//All of the entered details are saved in the user table database.
	public function update()
	{
		$date = new DateTime;
		$user = new User;
		$user->username = Input::get('username');
		$user->password = Hash::make (Input::get('password'));
		$user->email = Input::get('email');
		$user->created_at = $date;
		$user->updated_at = $date;
		$user->name = Input::get('name');
		$user->address = Input::get('address');
		$user->phone = Input::get('phone');
//		DB::table('users')->insert(array($inputs));
/* 		DB::insert('insert into users (username, password, email, created_at, 
				updated_at, name, address, phone) values (?, ?, ?,$date,$date, ?, ?, ?)', array($inputs));	 */
		$user->save();
		return Redirect::to('book');
		} 
		
		// This creates the member view
		public function makeMember(){
			return View::make('member');
		}
		
		//This creates the admin view
		public function makeAdmin(){
			
			return View::make('admin');
		}
}