<?php

/*
 * This is a RESTful controller similar to our Book controller.
 * It has the standard index,show,edit,update,create and destroy methods.
 * This controller is sending data to our three review views, show, index and edit.
 * It enables the members to review a book of their choice and then these details are sent to the review table in our database
 */
class ReviewController extends \BaseController {

// 		function __construct() {
// 		$this->beforeFilter('auth', array('except' =>
// 				array('index', 'show')));
// 	}

	// This function displays all the books for our index view
	public function index()
	{
		$books = Book::all();

		return View::make('review.index')->with('books', $books);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	//This function creates our show view with all our book objects
	public function show()
	{
		$lbooks = Book::all();

		return View::make('review.show')->with('books', $books);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	//This function enables the edit view to be displayed with the particular book by id
	public function edit($id)
	{
		$book = Book::find($id);
		return View::make('review.edit')->with('book',$book);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	//This function enables a review to be created with a new review object and the details entered by the user
	//are then updated in the review table in our database.
	public function update()
	{
		$review = new Review ();
		$review->rating = Input::get ( 'Review' );
		$review->book_id = Input::get ( 'bookId' );
		$review->comment = Input::get ( 'comments' );
		$review->save ();
		
		return Redirect::to ( 'member' );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}