@extends('layout')
<!-- Here we display the librarian details for processing a new loan and newly signed up member details  -->
@section('header')
	Librarians Page
@section('content')	
<a href="{{ URL::to('processLoan') }}">Process Loan</a>
<p>{{{ isset($message) ? $message : '' }}}</p>
@stop
<table>
<tr>
<th>id</th>
  <th>username</th>		
  <th>email</th>
  <th>phone</th>
  <th>Book_allowance</th>
</tr>	
@foreach($memberslist as $member)
<tr>
{{Form::open(array('url' => 'memberdetails'))}}
<td>{{$member->id}}</td>
  <td>{{$member->username}}</td>
  <td>{{$member->email}}</td>
  <td>{{$member->phone}}</td>
  <td>{{$member->Book_allowance}}</td>
  <td>{{Form::submit('Approve') }}</td>
	{{Form::close()}}
</tr>
@endforeach	
	</table>
@stop