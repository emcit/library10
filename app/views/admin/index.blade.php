@extends('layout')
<!-- This creates the layout for displaying the fines report with all the members and their outstanding fines--> 
@section('header')
	FINES REPORT:
@stop

@section('leftmenu')
	@parent
	<p><a href="{{URL::to('nowtime')}}">Current Time</a></p>
@stop

@section('content')
<head>
<style>
table,th,td
{
border:1px solid green;
}
</style>
</head>

<table>
<tr>
<th>Name: </th>
<th>No. times fined: </th>
<th>User Id: </th>
<th>Fines total(EUR): </th>
</tr>
	@foreach($result as $row)
		<tr>
		<td>{{{$row->name}}}</td>
		<td>{{{$row->times}}}</td>
		<td>{{{$row->userid}}}</td>
		<td>{{{$row->fines}}}</td>		
		</tr><br/>
	@endforeach
</table>
@stop
