@extends('layout')
<!-- Here we provide the layout with a form to process a loan and to return a book -->
@section('header')
	Please Record Loan
@stop

@section('content')
	{{Form::open(array('url' => 'loanUpdate'))}}
		<!-- List any login errors -->
		@if($errors->has())
			<ul>
				@foreach ($errors->all() as $error)
			 	<li>{{ $error }}</li>
				@endforeach
			</ul>
		@endif

		<p>
			{{Form::label('userid', 'UserId')}}
			{{Form::text('userid', '')}}
		</p>
		<p>
			{{Form::label('uniqueid', 'Uniqueid')}}
			{{Form::text('uniqueid', '')}}
		</p>

{{ Form::select('titles', Book::lists('title', 'id')) }}


		<p>{{Form::submit('Loan', array('name' => 'loan')) }} 
		 {{Form::submit('Return', array('name' => 'return')) }}</p>
		
	{{Form::close()}}
@stop