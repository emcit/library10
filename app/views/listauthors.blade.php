@extends('layout')

@section('header')
	Author List
@stop

@section('content')
	@foreach($authors as $author)
		{{$author->getName()}} <br/>
	@endforeach
@stop
